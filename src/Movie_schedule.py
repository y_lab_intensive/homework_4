from datetime import datetime, timedelta
from typing import Generator, List, Tuple


class Movie:
    def __init__(self, name, date) -> Generator:
        self.start, self.end = date.pop(0)
        self.current = self.start
        self.date = date

    def schedule(self) -> Generator[datetime, None, None]:
            while True:
                try:
                    yield self.current
                    self.current += timedelta(days=1)
                    if self.current > self.end:
                        if not self.date:
                            raise StopIteration
                        self.current, self.end = self.date.pop(0)
                except StopIteration:
                    break


if __name__ == '__main__':
    m = Movie('sw', [
        (datetime(2020, 1, 1), datetime(2020, 1, 7)),
        (datetime(2020, 1, 15), datetime(2020, 2, 7))
    ])

    for d in m.schedule():
        print(d)

