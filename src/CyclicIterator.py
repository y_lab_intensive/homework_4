

class CyclicIterator:

    def __init__(self, object):
        self.curr = object[0]
        self.stop = object[-1]
        self.len: int = 0
        self.i: int = 0
        self.object = object
        # self.value = self.start - self.step

    # get iteration to iterate over an object
    def __iter__(self):
        self.len = len(self.object)
        return self

    # moving to the next value and reading it
    def __next__(self):
        self.i += 1
        if self.i < self.len:
            return self.object[self.i-1]
        else:
            self.i = 0
            return self.object[-1]

def main():
    lis_x: list = [1, 2, 3]
    tuple_x: tuple = (1, 2, 4, 5)
    string_x: str = "Hello"
    range_x: range = range(1, 5, 2)

    d = CyclicIterator(range_x)

    for i in d:
        print(i)

if __name__ == '__main__':
    main()


